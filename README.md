This repository will be used as the website for Parallelism and Concurrency CS-206. It will be updated weekly throughout the semester. This README contains general information about the class.

- [previous-exams](previous-exams) contains PDFs for the previous exams.
- [recitation-sessions](recitation-sessions) contains markdown documents for recitation sessions and solutions.
- [slides](slides) contains the slides presented in class.

We will use GitLab's issue tracker as a discussion forum. Feel free to [open an issue](https://gitlab.epfl.ch/lamp/cs206-2020/issues) if you have any comments or questions.

# First-week tasks

1. [Click here to complete the Doodle to register to the recitation sessions](https://www.doodle.com/poll/aigekc58td679btt)
2. Follow the [Tools Setup](homeworks/instructions/tools-setup.md) page.
3. Do the [example homework](homeworks/instructions/example-homework.md).
4. Do the [first graded homework](homeworks/homework1-parallel-box-blur-filter/).

# Grading

The grading of the course is divided between homework (10%), a midterm exam (45%) and a final exam (45%) held during the last week of courses. The midterm will be a closed book written exam. The final exam will be a programming assignment on computers, similar to the homeworks.

# Staff

| Role        | People                                                                  |
| :--         | :--                                                                     |
| Professors  | Martin Odersky, Viktor Kunčak                                           |
| TAs         | Romain Edelmann, Guillaume Martres, Nicolas Stucki, Olivier Blanvillain |
| Student TAs | Antoine Sidem, Louis Vialar, Matteo Pariset                             |

# Rooms

~~Live lectures take place in CM 3. Recitation sessions take place in CM 011, CM 013, INM 200 and CM 3. Project sessions take place in INF 1 and INF 2.~~

**NEW** Discord server: https://discord.gg/8kjPuFU

# Lecture Schedule

| Week | Date     | Topic         | 14:15-16:00          | 16:15-18:00   |
| :--  | :--      | :--           | :--                  | :--           |
| 1    | 19.02.20 | Parallelism 1 | Live lecture         | Project 1     |
| 2    | 26.02.20 | Parallelism 2 | Recitation session 1 | Project 1 & 2 |
| 3    | 04.03.20 | Parallelism 3 | Recitation session 2 | Project 2 & 3 |
| 4    | 11.03.20 | Parallelism 4 | Recitation session 3 | Project 3 & 4 |
| 5    | 18.03.20 | Concurrency 1 | [Live lecture **on Zoom** (YouTube link)](https://www.youtube.com/watch?v=hwdtuzx2GvU) | Project 4 & 5 |
| 6    | 25.03.20 | Concurrency 2 | [Live lecture **on Zoom** (YouTube link)](https://www.youtube.com/watch?v=jcdJU6a3ArE) | Project 5 & 6 |
| 7    | 01.04.20 | Concurrency 3 | [Live lecture **on Zoom** (YouTube link)](https://www.youtube.com/watch?v=XznYvMjA-7s) | Project 6     |
| 8    | 08.04.20 | ~~Exam~~          |                      | ~~Midterm~~ **CANCELLED**       |
| 9    | 15.04.20 |               |                      |               |
| 10   | 22.04.20 | Actors 1      | Recitation session 4 | Project 7     |
| 11   | 29.04.20 | Actors 2      | Recitation session 5 | Project 7     |
| 12   | 06.05.20 | Spark 1       | Recitation session 6 | Project 8     |
| 13   | 13.05.20 | Spark 2       | Recitation session 7 | Project 8 & 9 |
| 14   | 20.05.20 | Spark 3       | Recitation session 8 | Project 9     |
| 15   | August (TBD) | Exam          | Final Exam           |               |

Recitation sessions are released prior to the live sessions, on Sundays at 16:00. Solutions are released a week later.

Before each recitation session, students should watch videos corresponding to that week's topic:

### Parallelism 1: Introduction to Parallel Programming

- [Introduction to Parallel Computing](https://www.youtube.com/watch?v=94O72nyNFY0)
- [Parallelism on the JVM I](https://www.youtube.com/watch?v=I8w-q1TPtjA)
- [Parallelism on the JVM II](https://www.youtube.com/watch?v=BbVWGWTNAXw)
- [Running Computations in Parallel](https://www.youtube.com/watch?v=KkMZGJ3M2-o)
- [Monte Carlo Method to Estimate Pi](https://www.youtube.com/watch?v=VBCf-aTgpPU)
- [First-Class Tasks](https://www.youtube.com/watch?v=mrVVaXCuhBc)
- [How Fast are Parallel Programs?](https://www.youtube.com/watch?v=Lpnexp_Qxgo)
- [Benchmarking Parallel Programs](https://www.youtube.com/watch?v=LvS_kjCssfg)

### Parallelism 2: Basic Task Parallel Algorithms

- [Parallel Sorting](https://www.youtube.com/watch?v=AcuvVgQbphg)
- [Data Operations and Parallel Mapping](https://www.youtube.com/watch?v=ghYtMLrphZw)
- [Parallel Fold (Reduce) Operation](https://www.youtube.com/watch?v=hEBgyhIoWww)
- [Associativity I](https://www.youtube.com/watch?v=q-Cl3whISCY)
- [Associativity II](https://www.youtube.com/watch?v=XBjqYavDUB8)
- [Parallel Scan (Prefix Sum) Operation](https://www.youtube.com/watch?v=CYr3YaQiMwo)

### Parallelism 3: Data-Parallelism

- [Data-Parallel Programming](https://www.youtube.com/watch?v=WW7TabCiOV8)
- [Data-Parallel Operations I](https://www.youtube.com/watch?v=Vd35YQ8DEO4)
- [Data-Parallel Operations II](https://www.youtube.com/watch?v=dcMgKtuAh3s)
- [Scala Parallel Operations](https://www.youtube.com/watch?v=NjkxjAT7ohE)
- [Splitters and Combiners](https://www.youtube.com/watch?v=Redz85Nlle4)

### Parallelism 4: Data-Structures for Parallel Computing

- [Implementing Combiners](https://www.youtube.com/watch?v=dTP0ntniB2I)
- [Parallel Two-phase Construction](https://www.youtube.com/watch?v=XcMtq3OdjQ0)
- [Conc-Tree Data Structure](https://www.youtube.com/watch?v=cUXHXKL8Xvs)
- [Amortized, Constant-Time Append Operation](https://www.youtube.com/watch?v=Ic5DUZLITVI)
- [Conc-Tree Combiners](https://www.youtube.com/watch?v=aLfFlCC1vjc)

### Concurrency 1

- [Full Lecture](https://www.youtube.com/watch?v=hwdtuzx2GvU)

### Concurrency 2

- [Full Lecture](https://epfl.zoom.us/rec/play/75YvceyoqjM3HoLAtASDVKR8W47ufK2s0HdK__APnke2B3BRY1auNbREZeJ3Lf-70FBEufrBat_DDxP_?continueMode=true)

### Concurrency 3

- [Full Lecture](https://epfl.zoom.us/rec/share/x9BSBbz21F1LE6ft43nHX4oRPdjceaa80yhM__NZmEfiDTYClPrsdkEuQoHHv_oZ)

### Actors 1

- [Introduction: why actors?](https://www.youtube.com/watch?v=ZQAe9AItH8o)
- [The Actor Model](https://www.youtube.com/watch?v=c49tDZuFtPA)

### Actors 2

- [Message Processing Semantics](https://www.youtube.com/watch?v=Uxn1eg6R0Fc)
- [Designing Actor Systems](https://www.youtube.com/watch?v=uxeMJLo3h9k)
- [Testing Actor Systems](https://www.youtube.com/watch?v=T_2nwLr-H2s)

### Spark 1: Spark Basics

- [From Parallel to Distributed](https://www.youtube.com/watch?v=bfMbJ8NzTZI)
- [Latency](https://www.youtube.com/watch?v=igNIz2Ent5E)
- [RDDs, Spark's Distributed Collection](https://www.youtube.com/watch?v=EuVmW62aIXI)
- [RDDs: Transformations and Actions](https://www.youtube.com/watch?v=qJlfATheS38)
- [Evaluation in Spark: Unlike Scala Collections!](https://www.youtube.com/watch?v=0pVYuuUrN74)
- [Cluster Topology Matters](https://www.youtube.com/watch?v=lS4vRzwrmtU)

### Spark 2: Reduction Operations & Distributed Key-Value Pairs

- [Reduction Operations](https://www.youtube.com/watch?v=JhF0_Ka_iqU)
- [Pair RDDs](https://www.youtube.com/watch?v=kIUzgweDMUs)
- [Transformations and Actions on Pair RDDs](https://www.youtube.com/watch?v=ovf0GFbnp5g)
- [Joins](https://www.youtube.com/watch?v=kYpaZpj4qTM)

### Spark 3: Partitioning and Shuffling

- [Shuffling: What it is and Why it’s important](https://www.youtube.com/watch?v=LrgA4PrKrks)
- [Partitioning](https://www.youtube.com/watch?v=sTcki6mxjcA)
- [Optimizing with Partitioners](https://www.youtube.com/watch?v=4Vfp5kp2jnE)
- [Wide vs Narrow Dependencies](https://www.youtube.com/watch?v=L9BnaYp10c8)


# Homework Schedule

| Project number | Name                       | Start date | Due date (23:59 [AoE](https://en.wikipedia.org/wiki/Anywhere_on_Earth)) |
| :--            | :--                        | :--        | :--                  |
| 1              | Parallel Box Blur Filter   | 17.02.20   | 01.03.20             |
| 2              | Reductions and Prefix Sums | 24.02.20   | 08.03.20             |
| 3              | K-Means                    | 02.03.20   | **UPDATED: 22.03.20**             |
| 4              | Barnes-Hut Simulation      | 09.03.20   | **UPDATED: 29.03.20**             |
| 5              | Bounded Buffer             | 16.03.20   | **UPDATED: 05.04.20**             |
| 6              | Lock-free Sorted List      | 23.03.20   | **UPDATED: 13.04.20**             |
| 7              | Actors Binary Tree         | 13.04.20   | 03.05.20             |
| 8              | Wikipedia                  | 04.05.20   | 17.05.20             |
| 9              | StackOverflow              | 11.05.20   | 24.05.20             |

# Exam Schedule

The final exam will take place in August, the precise date is still to be determined. The final exam will cover all material seen during the semester.

Information about exams organization will be communicated by email.
